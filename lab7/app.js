const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');


mongoose.connect('mongodb://localhost/laboratoire10TodoList', { useMongoClient: true }, (error) => {
	if(error) {
	    console.error(error.message);
	    process.exit(1);
	}
	else {
		console.log("Connection to mongodb successful");
	}
});

mongoose.Promise = global.Promise;

const corsOptions = {
    origin: '*',
    methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE', 'UPDATE'],
    credentials: true
};
const uuidv4 = require('uuid/v4');

const port = 8080;
const app = express();

const users = [];
const userTasks = {};

const TodoList = mongoose.model('TodoList',
    {
        user: String,
        tasks: [{
            id: String,
            name: String
        }]
    });

app.use(bodyParser.json());
app.use(cors(corsOptions));

app.use(function (error, req, res, next) {
    if (error instanceof SyntaxError) {
        res.status(400).send({
            errorCode: 'PARSE_ERROR',
            message: 'Arguments could not be parsed, make sure request is valid.'
        });
    } else {
        res.status(500).send('Something broke server-side.', error);
    }
});

app.get('/', function(req, res) {
    res.send('Welcome to Lab 4 API.');
});

app.post('/users', function(req, res) {
	const userId = uuidv4();

    const user = new TodoList({
        user: userId,
        tasks: []
    });

    user.save((error) => {
        if (error)
            console.error(error.message);
    });

	return res.status(200).send(JSON.stringify({'id': userId}));
});

app.get('/:userId/tasks', function(req, res) {
    const userId = req.params.userId;

    ensureUserExist(userId, res, function() {
        getUserTasks(userId, function(tasks) {
	        return res.status(200).send(JSON.stringify({'tasks': tasks}));
        });
    });
});

app.post('/:userId/tasks', function(req, res) {
    const userId = req.params.userId;

    ensureUserExist(userId, res, function() {
        ensureValidTask(req.body, res, function() {
	        const task = {id: uuidv4(), name: req.body.name};
            TodoList.update({user: userId}, { $push: { tasks: task } }, (err, task) => {
                if (err) console.error(err);
            });

            return res.status(200).send(JSON.stringify(task));
        });
    });

});

app.put('/:userId/tasks/:taskId', function(req, res) {
    const taskId = req.params.taskId;
    const userId = req.params.userId;

    ensureUserExist(userId, res, function() {
        ensureValidTask(req.body, res, function() {
            editTasks(userId, taskId, res, function(task){
	            TodoList.update(
		            { user: userId, "tasks.id": taskId },
                    { $set: { "tasks.$.name": req.body.name} },
		            (error) => {
			            if (error) console.error(error.message);
		            });
	            return res.status(200).send(JSON.stringify(task));
            });
        });
    });
});

app.delete('/:userId/tasks/:taskId', function(req, res) {
	const taskId = req.params.taskId;
	const userId = req.params.userId;

	ensureUserExist(userId, res, function() {
		editTasks(userId, taskId, res, function(task){
			TodoList.update(
				{ user: userId},
				{ "$pull": {tasks : { id: taskId } } },
				{ "multi" : true },
				(error) => {
				    if (error) console.error(error.message);
                });
			return res.sendStatus(204);
		});
	});
});

app.listen(port, function() {
    console.log('Server listening.')
});

function ensureValidTask(task, res, callback) {
    if (task.name === undefined || task.name === '') {
        return res.status(400).send('Task definition is invalid.');
    }

    callback();
}

function ensureUserExist(userId, res, callback) {
    TodoList.find({user: userId}, (error, users) => {
        if (users.length <= 0)
            return res.status(400).send('User with id \'' + userId + '\' doesn\'t exist.');
        else {
	        callback();
        }
    });
}

function editTasks(userId, taskId, res, callback) {
    TodoList.findOne({user: userId}, (error, task) => {
        if (error) return res.status(400).send('Task with id \'' + taskId + '\' doesn\'t exist.');
        else {
            callback(task);
        }
    });
}

function getUserTasks(userId, callback) {
	TodoList.find({user: userId}, (error, users) => {
		callback(users[0].tasks)
	});
}
